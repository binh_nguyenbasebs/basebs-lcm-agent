import React from 'react'

import Headers from '../components/header/header'

import TaskList from '../components/sidebar/sidebar'

import TabCard from '../components/tabs/tabs'

import Footers from '../components/footer/footer'

import { Container, Header, Content, Footer, Sidebar } from 'rsuite'

const LCMAgent = () => {
  return (
    <>
      <Container>
        <Header>
          <Headers />
        </Header>
        <Container>
          <Sidebar>
            <TaskList />
          </Sidebar>
          <Content>
            <TabCard />
          </Content>
        </Container>
        <Footer>
          <Footers />
        </Footer>
      </Container>
    </>
  )
}

export default LCMAgent
