import React from 'react';
import logo from './logo.svg';
import './App.css';
import './css/custom.scss'

import LCMAgent from './lcmAgent/lcmAgent'

function App() {
  return (
      <>
      <LCMAgent />
      </>
  );
}

export default App;
