import React, { useState } from 'react'

import 'antd/dist/antd.css'

import { Tabs } from 'antd'

import { Button as RsuiteBtn, IconButton } from 'rsuite'

import './tabs.scss'

import callicon from '../../assets/images/tasklist/callicon.svg'

import editpencilicon from '../../assets/images/tasklist/edit.svg'

import dimensionedit from '../../assets/images/tasklist/dimensionEdit.svg'

import callicongr from '../../assets/images/tasklist/callicongreen.svg'

import contactic from '../../assets/images/tasklist/contactic.svg'

import calendar from '../../assets/images/tasklist/calendar.svg'

import location from '../../assets/images/tasklist/location.svg'

import callbook from '../../assets/images/tasklist/callbook.svg'

import Information from './information'

import BusinessResult from './businessResult'

import CallResult from './callResult'

const initialPanes = [
  {
    title: 'Nguyễn Minh Triết',
    content: 'Content of Tab 1',
    key: '1'
  },
  {
    title: 'Cao Thanh Hùng',
    content: 'Content of Tab 2',
    key: '2'
  },
  {
    title: 'Nguyễn Như Ngọc',
    content: 'Content of Tab 3',
    key: '3'
  }
]

const TabCard = () => {
  const [activeKey, setActiveKey] = useState(initialPanes[0].key)

  const [panes, setPanes] = useState(initialPanes)

  const { TabPane } = Tabs

  const onChange = (activeKey) => {
    setActiveKey(activeKey)
  }

  //   const onEdit = (targetKey, action) => {
  //     this[action](targetKey);
  //   };

  return (
    <>
      <Tabs
        hideAdd
        type="editable-card"
        onChange={() => onChange()}
        activeKey={activeKey}
        //onEdit={this.onEdit}
      >
        {panes.map((pane) => (
          <TabPane tab={pane.title} key={pane.key} closable={pane.closable}>
            {/* {pane.content} */}
            <div className="tabpanel">
              <div>
                <RsuiteBtn
                  //   id="addallcondition"
                  style={{ marginBottom: '0.8rem' }}
                  //   onClick={() => addAll()}
                  className="notcalledbtn"
                >
                  <img
                    src={callicon}
                    style={{ marginRight: '0.9rem', width: '2rem' }}
                  />
                  <span style={{ marginRight: '0.9rem', fontSize: '1.2rem' }}>
                    Call
                  </span>
                  <span style={{ marginRight: '0.9rem', fontSize: '1rem' }}>
                    (+84) 123456789 234
                  </span>
                </RsuiteBtn>
              </div>
              <div className="row">
                <div className="col-lg-6 col-md-6 hasborder">
                  <Information />
                </div>
                <div className="col-lg-6 col-md-6">
                  <div className="col-lg-12 col-md-12">
                    <BusinessResult />
                  </div>
                  <div className="col-lg-12 col-md-12">
                    <CallResult />
                  </div>
                </div>
              </div>
            </div>
          </TabPane>
        ))}
      </Tabs>
    </>
  )
}

export default TabCard
