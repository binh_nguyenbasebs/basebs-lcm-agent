import React from 'react'

import './tabs.scss'

import callicon from '../../assets/images/tasklist/callicon.svg'

import editpencilicon from '../../assets/images/tasklist/edit.svg'

import dimensionedit from '../../assets/images/tasklist/dimensionEdit.svg'

import callicongr from '../../assets/images/tasklist/callicongreen.svg'

import contactic from '../../assets/images/tasklist/contactic.svg'

import calendar from '../../assets/images/tasklist/calendar.svg'

import location from '../../assets/images/tasklist/location.svg'

import callbook from '../../assets/images/tasklist/callbook.svg'

import { IconButton } from 'rsuite'

const Information = () => {
  return (
    <>
      <div className="titlesection row">
        <div className="col-lg-4 col-md-4" style={{ top: '0.4rem' }}>
          <span style={{ alignSelf: 'center' }}>Information</span>
        </div>
        <div className="col-lg-8 col-md-8">
          <div className="inlineflex" style={{ float: 'right' }}>
            <IconButton size="md" icon={<img src={editpencilicon} />} />
            <IconButton
              size="md"
              icon={
                <img
                  src={dimensionedit}
                  // onClick={() => setModalEditDimension(true)}
                />
              }
            />
          </div>
        </div>
      </div>
      <div className="bodysection">
        <div className="row">
          <div className="col-lg-2 col-md-2">
            <img src={contactic} />
          </div>
          <div className="col-lg-10 col-md-10">
            <span className="label">Customer name:</span>
            <span className="value">Cao Thanh Hùng</span>
          </div>
        </div>
        <div className="row">
          <div className="col-lg-2 col-md-2">
            <img src={callicongr} style={{ width: '1.1rem' }} />
          </div>
          <div className="col-lg-10 col-md-10">
            <span className="label">Contact:</span>
            <span className="value">(+84) 1234567890</span>
          </div>
        </div>
        <div className="row">
          <div className="col-lg-2 col-md-2">
            <img src={callbook} />
          </div>
          <div className="col-lg-10 col-md-10">
            <span className="label">Amount:</span>
            <span className="value">24</span>
          </div>
        </div>
        <div className="row">
          <div className="col-lg-2 col-md-2">
            <img src={calendar} />
          </div>
          <div className="col-lg-10 col-md-10">
            <span className="label">Due Date:</span>
            <span className="value">25/10/2020</span>
          </div>
        </div>
        <div className="row">
          <div className="col-lg-2 col-md-2">
            <img src={location} />
          </div>
          <div className="col-lg-10 col-md-10">
            <span className="label">Province:</span>
            <span className="value">Tien Giang</span>
          </div>
        </div>
      </div>
    </>
  )
}

export default Information
