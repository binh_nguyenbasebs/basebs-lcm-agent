import React from 'react'

import { IconButton } from 'rsuite'

import './tabs.scss'

import callicon from '../../assets/images/tasklist/callicon.svg'

import editpencilicon from '../../assets/images/tasklist/edit.svg'

import dimensionedit from '../../assets/images/tasklist/dimensionEdit.svg'

import callicongr from '../../assets/images/tasklist/callicongreen.svg'

import contactic from '../../assets/images/tasklist/contactic.svg'

import calendar from '../../assets/images/tasklist/calendar.svg'

import location from '../../assets/images/tasklist/location.svg'

import callbook from '../../assets/images/tasklist/callbook.svg'

import { Form, Input, Checkbox } from 'antd'

const BusinessResult = () => {
  return (
    <>
      <div className="hasborder" style={{marginBottom:'0.8rem'}}>
        <div className="titlesection row">
          <div className="col-lg-4 col-md-4" style={{ top: '0.4rem' }}>
            <span style={{ alignSelf: 'center' }}>Business result</span>
          </div>
          <div className="col-lg-8 col-md-8">
            <div className="inlineflex" style={{ float: 'right' }}>
              <IconButton size="md" icon={<img src={editpencilicon} />} />
              <IconButton
                size="md"
                icon={
                  <img
                    src={dimensionedit}
                    // onClick={() => setModalEditDimension(true)}
                  />
                }
              />
            </div>
          </div>
        </div>
        <div className="bodysection">
          <div className="row">
            <Form
              id="formbusinessresult"
              labelCol={{ span: 12 }}
              wrapperCol={{ span: 10 }}
              layout="horizontal"
              // initialValues={{ size: componentSize }}
              // onValuesChange={onFormLayoutChange}
              // size={componentSize}
            >
              <Form.Item label="Ngày hẹn trả">
                <Input />
              </Form.Item>
              <Form.Item label="Remark">
                <Checkbox />
              </Form.Item>
              <Form.Item label="Thái độ khách hàng">
                <Input />
              </Form.Item>
            </Form>
          </div>
        </div>
      </div>
    </>
  )
}

export default BusinessResult
