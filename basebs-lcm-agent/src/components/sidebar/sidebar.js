import React from 'react'

import './sidebar.scss'

import calendar from '../../assets/images/tasklist/icon_calendar.svg'

import called from '../../assets/images/tasklist/called.svg'

import notcalled from '../../assets/images/tasklist/notcalled.svg'

const TaskList = () => {
  return (
    <>
      <div id="tasklistcon">
        <div id="tasklisttitle">
          <div className="row">
            <div className="col-lg-3 col-md-3">
              <img src={calendar} />
            </div>
            <div className="col-lg-9 col-md-9">Task list</div>
          </div>
        </div>
        <div id="tasklist">
          <div className="tasklistcard called">
            <div className="row">
              <div className="col-lg-2 col-md-2">
                <img style={{ width: '2.5rem' }} src={called} />
              </div>
              <div className="col-lg-10 col-md-10">
                <div>Nguyễn Minh Triết</div>
                <div>(+84) 123456789</div>
              </div>
            </div>
          </div>
          <div className="tasklistcard notcalled">
            <div className="row">
              <div className="col-lg-2 col-md-2">
                <img style={{ width: '2.5rem' }} src={notcalled} />
              </div>
              <div className="col-lg-10 col-md-10">
                <div>Nguyễn Minh Triết</div>
                <div>(+84) 123456789</div>
              </div>
            </div>
          </div>
          <div className="tasklistcard called">
            <div className="row">
              <div className="col-lg-2 col-md-2">
                <img style={{ width: '2.5rem' }} src={called} />
              </div>
              <div className="col-lg-10 col-md-10">
                <div>Nguyễn Minh Triết</div>
                <div>(+84) 123456789</div>
              </div>
            </div>
          </div>
          <div className="tasklistcard notcalled">
            <div className="row">
              <div className="col-lg-2 col-md-2">
                <img style={{ width: '2.5rem' }} src={notcalled} />
              </div>
              <div className="col-lg-10 col-md-10">
                <div>Nguyễn Minh Triết</div>
                <div>(+84) 123456789</div>
              </div>
            </div>
          </div>
          <div className="tasklistcard called">
            <div className="row">
              <div className="col-lg-2 col-md-2">
                <img style={{ width: '2.5rem' }} src={called} />
              </div>
              <div className="col-lg-10 col-md-10">
                <div>Nguyễn Minh Triết</div>
                <div>(+84) 123456789</div>
              </div>
            </div>
          </div>
          <div className="tasklistcard notcalled">
            <div className="row">
              <div className="col-lg-2 col-md-2">
                <img style={{ width: '2.5rem' }} src={notcalled} />
              </div>
              <div className="col-lg-10 col-md-10">
                <div>Nguyễn Minh Triết</div>
                <div>(+84) 123456789</div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  )
}

export default TaskList
