import Avatar from '@material-ui/core/Avatar'
import Badge from '@material-ui/core/Badge'
import React from 'react'
import { Button, Popover, Whisper, Dropdown } from 'rsuite'
//import Cookies from 'universal-cookie'
import menubar from '../../assets/icons/menubar.svg'
import avatar from '../../assets/images/Avatar.png'
import camera from '../../assets/images/Camera.svg'
import logobase from '../../assets/images/BaseBSWhite.png'
import notification from '../../assets/images/Notification.png'
import './header.scss'
import { ButtonGroup } from '@material-ui/core'

const Header = () => {
  let tmpuserName = 'Nguyễn Văn A'

  let pagetitle = 'Test'

  return (
    <>
      <div id="headercontainer">
        <div id="logocon">
          <img src={logobase}></img>
        </div>
        <div id="headerbar">
          <div id="titlepage">
            <Button style={{marginRight:'1rem'}}>Block/Unblock</Button>
            <Dropdown
              title="Ready"
              toggleComponentClass={Button}
              appearance="default"
            >
              <Dropdown.Item>Ready</Dropdown.Item>
              <Dropdown.Item>Not ready</Dropdown.Item>
            </Dropdown>
          </div>
          <div id="infocon">
            <span>
              <img
                src={notification}
                alt=""
                //   style={{ width: '3rem' }}
                className="img-noti"
              ></img>
            </span>
            {/* <span><span id="divideline"></span></span> */}
            <span id="usernametitle">{tmpuserName}</span>
            <div id="avatarcon">
              <Whisper
                enterable
                trigger="hover"
                placement={'bottomEnd'}
                speaker={
                  <Popover
                    className="popoveravatar"
                    style={{ height: 'fit-content' }}
                  >
                    {/* <span>Test</span>
                             <span>Test</span> */}
                    <div className="avatarbadgecon">
                      <Badge
                        className="badgecon"
                        overlap="circle"
                        anchorOrigin={{
                          vertical: 'bottom',
                          horizontal: 'right'
                        }}
                        badgeContent={<img src={camera} alt=""></img>}
                      >
                        <Avatar
                          className="avatarbadge"
                          alt="Travis Howard"
                          src={avatar}
                        />
                      </Badge>
                    </div>
                    <div id="avatarconusername">
                      <span
                        style={{
                          fontSize: '1.2rem',
                          letterSpacing: '1px',
                          marginTop: '0.5rem'
                        }}
                      >
                        {tmpuserName}
                      </span>
                    </div>
                    <div id="avatarconemail">
                      <span
                        style={{
                          fontWeight: '300',
                          fontSize: '1rem',
                          marginTop: '0.5rem'
                        }}
                      >
                        {tmpuserName}
                      </span>
                    </div>
                    <div id="accountsettingbtn">
                      <div>
                        <Button block>Account Setting</Button>
                      </div>
                    </div>
                    <div id="signoutbtn">
                      <Button block>Sign Out</Button>
                    </div>
                  </Popover>
                }
              >
                <img src={avatar} alt=""></img>
              </Whisper>
            </div>
          </div>
        </div>
      </div>
    </>
  )
}

export default Header
