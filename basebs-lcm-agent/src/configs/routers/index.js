export const routersNotAuth = [
  {
    exact: true,
    path: '/login',
    component: 'login'
  }
]

export const routersAuth = [
  {
    exact: true,
    path: '/sharing',
    component: 'sharing',
    root: true,
    requirementPermission: []
  },
  {
    exact: true,
    path: '/workflows-setting',
    component: 'workflowSetting',
    root: true,
    requirementPermission: []
  },
  {
    exact: true,
    path: '/pick-list-dependence',
    component: 'pickList',
    root: true,
    requirementPermission: []
  },
  {
    exact: true,
    path: '/account',
    component: 'account',
    root: true,
    requirementPermission: []
  },
  {
    exact: true,
    path: '/users',
    component: 'users',
    root: true,
    requirementPermission: []
  },
  {
    exact: true,
    path: '/profiles',
    component: 'profiles',
    root: true,
    requirementPermission: []
  },
  {
    exact: true,
    path: '/newprofile',
    component: 'newProfile',
    root: true,
    requirementPermission: []
  },
  {
    exact: true,
    path: '/editprofile',
    component: 'editProfile',
    root: true,
    requirementPermission: []
  },
  {
    exact: true,
    path: '/roles',
    component: 'roles',
    root: true,
    requirementPermission: []
  },
  {
    exact: true,
    path: '/roles3',
    component: 'roles3',
    root: true,
    requirementPermission: []
  },
  {
    exact: true,
    path: '/newrole',
    component: 'newRole',
    root: true,
    requirementPermission: []
  },
  {
    exact: true,
    path: '/ticket',
    component: 'ticket',
    root: true,
    requirementPermission: []
  },
  {
    exact: true,
    path: '/editrole',
    component: 'editRole',
    root: true,
    requirementPermission: []
  },
  {
    exact: true,
    path: '/kanban',
    component: 'kanban',
    root: true,
    requirementPermission: []
  },
  {
    exact: true,
    path: '/groups',
    component: 'groups',
    root: true,
    requirementPermission: []
  },
  {
    exact: true,
    path: '/newgroup',
    component: 'newgroup',
    root: true,
    requirementPermission: []
  },
  {
    exact: true,
    path: '/ticketlists',
    component: 'ticketlists',
    root: true,
    requirementPermission: []
  },
  {
    exact: true,
    path: '/editgroup',
    component: 'editgroup',
    root: true,
    requirementPermission: []
  },
  // {
  //     exact: true,
  //     path: '/tenants',
  //     component: 'tenants',
  //     root: true,
  //     requirementPermission: []
  // },
  {
    exact: true,
    path: '/objectsetting',
    component: 'objectsetting3',
    root: true,
    requirementPermission: []
  },
  {
    exact: true,
    path: '/workflows',
    component: 'workflows',
    root: true,
    requirementPermission: []
  },
  {
    exact: true,
    path: '/tenants',
    component: 'tenants',
    root: true,
    requirementPermission: []
  },
  {
    exact: true,
    path: '/objects',
    component: 'objects',
    root: true,
    requirementPermission: []
  },
  {
    exact: true,
    path: '/dashboard',
    component: 'dashboard',
    root: true,
    requirementPermission: []
  },
  {
    exact: true,
    path: '/lcm_droplist',
    component: 'components/DropListConfig'
  },
  {
    path: '/ticketdetail',
    component: 'ticketDetail',
    root: true,
    requirementPermission: []
  },
  {
    path: '/object',
    component: 'object',
    root: true,
    requirementPermission: []
  },
  {
    exact: true,
    path: '/report',
    component: 'report',
    root: true,
    requirementPermission: []
  },
  {
    exact: true,
    path: '/lcm-mode-config',
    component: 'containers/ModeConfig'
  },
  {
    exact: true,
    path: '/lcm-campaign-group',
    component: 'containers/CampaignGroup'
  },
  {
    exact: true,
    path: '/lcm-strategy-config',
    component: 'containers/StrategyConfig'
  },
  {
    exact: true,
    path: '/lcm-upload-data',
    component: 'containers/UploadData'
  },
  {
    path: '/lcm_business_information',
    component: 'components/businessInformationConfig'
  },
  {
    exact: true,
    path: '/lcm_business_result',
    component: 'components/businessResultConfig'
  },
  {
    exact: true,
    path: '/lcm_schedule',
    component: 'components/scheduleConfig'
  },
  {
    exact: true,
    path: '/lcm_campaign',
    component: 'components/campaignConfig'
  },
  {
    path: '/related-object',
    component: 'relatedObject',
    root: true,
    requirementPermission: []
  }
]
